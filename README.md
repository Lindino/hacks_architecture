Arquitetura Hacks
=================================================

COMO USAR:
----------------------------------------------
1 - Abrir projeto na IDE;

2 - Abrir o código da Interface;

3 - Ir no código-fonte da Interface;

4 - Mudar os caminhos para abrir arquivos:

	- file_out_macro;

	- file_out_montador;

	- file_out_ligador;

5 - Executar a Interface;

6 - Clicar no file e selecionar Codigo_1;

7 - Apertar em arquivo_1 na Interface;

8 - Clicar no file e selecionar Codigo_2;

9 - Apertar em arquivo_2 na Interface;

10 -Apertar o botão Executar;

*Os botões Macro, Ligador e Montador mostra o que cada etapa gerou;